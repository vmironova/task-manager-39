package ru.t1consulting.vmironova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.Session;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @NotNull
    public final static Session USER_SESSION1 = new Session();

    @NotNull
    public final static Session USER_SESSION2 = new Session();

    @NotNull
    public final static Session USER_SESSION3 = new Session();

    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Session> USER_SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2, USER_SESSION3);

    @NotNull
    public final static List<Session> SESSION_LIST = Arrays.asList(USER_SESSION1, USER_SESSION2);

    static {
        USER_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
    }

}
