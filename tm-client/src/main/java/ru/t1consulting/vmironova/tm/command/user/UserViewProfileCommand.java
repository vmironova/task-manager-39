package ru.t1consulting.vmironova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.request.UserViewProfileRequest;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final User user = getAuthEndpoint().viewProfileUser(request).getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
